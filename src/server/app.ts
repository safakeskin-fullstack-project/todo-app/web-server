import express from "express";
import { json, urlencoded } from "body-parser";
import cors from "cors";

// Add a list of allowed origins.
// If you have more origins you would like to add, you can add them to the array below.
const allowedOrigins = ["http://localhost:3000"];

const options: cors.CorsOptions = {
  origin: allowedOrigins,
};

export const app = express();
app.use(cors(options));
app.use(urlencoded({ extended: false }));
app.use(json());

export default app;
