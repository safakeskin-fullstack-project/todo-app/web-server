import app from "./app";

import todoRouter from "./routes/todo";

app.use("/todo", todoRouter);

export const start = (port: number | string = 3000) =>
  new Promise((resolve, reject) => {
    app.listen(port, () => resolve(`http://localhost:${port}`));
    app.on("error", reject);
  });

export default app;
