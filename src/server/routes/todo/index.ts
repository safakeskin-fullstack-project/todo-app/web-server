import express from "express";
import controller from "../../controllers/todo";

const router = express.Router();

router.get("/", controller.getTodoList);
router.post("/", controller.createTodo);
router.put("/", controller.updateTodo);

export default router;
