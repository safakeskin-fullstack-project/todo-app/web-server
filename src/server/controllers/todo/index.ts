import log from "loglevel";
import { Request, Response } from "express";
import connectionConfig from "../../../db";
import { Todo } from "../../../db/entities/todo";
import { controllerFlow } from "../../../util";
import { QueryDeepPartialEntity } from "typeorm/query-builder/QueryPartialEntity";

export type TodoItemUpdateParams = {
  id: string;
};

export type TodoItemCreatePayload = {
  value: string;
  completed: boolean;
};

const createTodo = async (req: Request, res: Response) => {
  const { value, completed } = req.body as TodoItemCreatePayload;
  const entityManager = connectionConfig.connection!.manager;

  const entity = await entityManager.create(Todo, { value, completed });
  const createdEntity = await entityManager.save(entity);

  log.info(`Created new activity: ${JSON.stringify(createdEntity)}`);
  res.status(201).send(createdEntity);
};

const updateTodo = async (req: Request, res: Response) => {
  const { id } = req.query as TodoItemUpdateParams;
  const newEntity = req.body as QueryDeepPartialEntity<Todo>;
  const entityManager = connectionConfig.connection!.manager;

  const { affected } = await entityManager.update(
    Todo,
    { id },
    newEntity as QueryDeepPartialEntity<Todo>
  );

  log.info(`${affected} entity is updated with id: ${id}`);
  res.status(200).send();
};

const getTodoList = async (req: Request, res: Response) => {
  const entityManager = connectionConfig.connection!.manager;

  const todoList = await entityManager.find(Todo, { order: { id: "ASC" } });

  log.info(`Read todoList items: `);
  log.info(todoList);

  res.send(todoList);
};

// const showLeaderboard = async (req: Request, res: Response) => {
// }

export default {
  createTodo: controllerFlow(createTodo),
  getTodoList: controllerFlow(getTodoList),
  updateTodo: controllerFlow(updateTodo),
};
