import dotenv from "dotenv";
import log from "loglevel";
import { start as startServer } from "./server";
import { makeConnection } from "./db";

dotenv.config();
log.setLevel("DEBUG");

export interface ProcessEnv {
  CONNECTION_NAME: string;
  SERVER_PORT: string;
  [key: string]: string | undefined;
}

const main = async (env: ProcessEnv) => {
  try {
    const connection = await makeConnection(env.CONNECTION_NAME);
    if (!connection.isConnected) {
      throw new Error(`isConnected flag is 'false' which indicates a problem.`);
    }
    log.debug(`'${env.CONNECTION_NAME}' db connection established.`);
  } catch (e) {
    log.error(`Error occured while establishing db connection: ${e}`);
    log.info("Process will be terminated due to the error written above.");
    return;
  }

  try {
    const serverUrl = await startServer(env.SERVER_PORT);
    log.debug(`Server is listening at: ${serverUrl}`);
  } catch (e) {
    log.error(`Error occured while starting server: ${e}`);
  }
};

main(process.env as ProcessEnv);
