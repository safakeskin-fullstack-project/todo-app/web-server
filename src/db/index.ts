import "reflect-metadata";
import { Connection, createConnection } from "typeorm";

export interface ConnectionConfig {
  connection?: Connection;
}

const connectionConfig: ConnectionConfig = {
  connection: undefined,
};

export const makeConnection = async (name: string) => {
  const connection = await createConnection(name);
  connectionConfig.connection = connection;
  return connection;
};

export default connectionConfig;
