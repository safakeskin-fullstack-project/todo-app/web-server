import { Request, Response } from "express";
import log from "loglevel";

export const controllerFlow = <T = void>(
  cb: (req: Request, res: Response) => Promise<T>
) => {
  const handler = async (req: Request, res: Response) => {
    try {
      return await cb(req, res);
    } catch (e) {
      log.error(
        `Error occured while handling ${req.method} with originalUrl: ${req.originalUrl}: ${e}`
      );
      res.status(500).send(e);
    }
  };
  return handler;
};
