## Start ngrok

```zsh
# ngrok http <port>
ngrok http 3000
```

## Start basic postgres db

```zsh
# [sudo] docker run -p 5432:5432 --name <container_name>
#   -e POSTGRES_USER=<user> -e POSTGRES_PASSWORD=<pass>
#   -d postgres

sudo docker run -p 5432:5432 --name db -e POSTGRES_USER=db -e POSTGRES_PASSWORD=db -d postgres
```

and you can get into shell of the container with below command

```zsh
sudo docker exec -it db bash
```
